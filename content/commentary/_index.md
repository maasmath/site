+++
title = "Commentary"

description = ""

insert_anchor_links = "left"
+++
Comments about the inner workings of our little collective as well as writeups on any
Mathematical topics we fancy.
