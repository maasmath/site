+++
title = "Why Math?"
date = 2019-08-31
description = "Why we chose to form MaasMath"
author = "Chris"
+++
It should go without saying mathematics is a thoroughly fascinating field. At its
core, it is the study of how to solve problems and how to then generalize those
solutions onto other problems. For a quick example, graphs can be used to model
anything from [social networks of little cute desert
hamsters](https://www.sciencedirect.com/science/article/pii/S000334721300153X?via%3Dihub)
to [scheduling problems](https://en.wikipedia.org/wiki/Graph_coloring#Scheduling) and
[complex automated reasoning](https://en.wikipedia.org/wiki/Bayesian_network). The
methodologies differ, of course, but basic mathematical tools like linear operations
on adjacency matrices are easily transferred between them. If we dig a bit deeper in
other domains and find ourselves in the land of algebraic topology, we can wind up
juggling geometric and algebraic properties of abstract objects in order to
[beautifully solve concrete problems](https://www.youtube.com/watch?v=AmgkSdhK4K8).
And it provably works!

Moving onto hot things like AI research _(DKE bias alert)_, if you want to have any
constructive input _you really need to know your maths_. Have a glance at papers from
the cool kids conferences (e.g.
[NeurIPS](https://papers.nips.cc/book/advances-in-neural-information-processing-systems-31-2018),
[AISTATS](http://proceedings.mlr.press/v84/),
[AAAI](https://aaai.org/Library/AAAI/aaai19contents.php)) and note that they're all
heavy on analysis and statistics, both of which are difficult to master without
proper guidance.

## Why MaasMath?
Having said all of those nice things about mathematics, note now that your options
for in depth study are a tad limited in the little town of Maastricht. Not enough
mathematics courses for a full BSc curriculum and no real associations to support you
in this endeavour.

This is where we'd like to come in.

We like studying. We like maths. We want to do research, and we like doing cool stuff
together.

Cheers to that.
