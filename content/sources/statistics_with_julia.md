+++
title = "Statistics with Julia"

description = "Intro to stats with a cutting edge programming language"

extra.type = "book"
+++
We went through a few chapters of [Statistics with Julia by Hayden Klok and Yoni
Nazarathy](https://people.smp.uq.edu.au/YoniNazarathy/julia-stats/StatisticsWithJulia.pdf),
a book meant as an introduction to stats with an additional pinch of
[Julia](https://julialang.org/). As of writing it is still a draft, and thus it being
a tad rough around the edges was expected.

The main benefit of this work is the implicit intro to Julia. It's a very powerful
language, and getting into it with the statistical examples definitely made its uses
feel more tangible. Nonetheless, as of now we cannot say this volume is an ideal
introduction to the field of Statistics, mainly due to being unfinished.
