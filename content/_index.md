+++
title = "About"

description = ""
+++
We are a community of Maastricht University students with a deep appreciation for the
highest domain of human intellect &ndash; Mathematics!

The goal of our little collective is to help one another learn the field by
weekly study sessions and group readings. In addition, we like to just have fun as a group in 
form of board nights or similar events!
You do NOT need to study a specific course to join MaasMath! 
We appreciate anyone with an interest in mathematics and it is a goal of ours 
to make mathematics fun and approachable.

For the coming academic year, we want to focus on "Stochastic Processes". We will be 
studying the mathematical background and plan to apply our knowledge to develop a good model 
for the dynamics of a stock we choose.

If this sounds like something you'd be interested in, don't be afraid to [join our
WhatsApp](/contact). 


This website is hosted on [GitLab](https://gitlab.com/maasmath/site), and uses the
lovely [Playfair Display](https://github.com/clauseggers/Playfair-Display) and [Libre
Baskerville](https://github.com/impallari/Libre-Baskerville) fonts.
