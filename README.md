# site
This repo contains the main Math Soc website. It is generated with
[Zola](https://getzola.org/) and lives on Gitlab Pages.

## Development
This is a Zola site, so the ultimate source of wisdom ought to be the [Zola
docs](https://www.getzola.org/documentation/content/overview/). However, below you
can see a brief rundown of how to operate it and how it works.

### Running it locally
To set it up locally [install Zola](https://www.getzola.org/documentation/getting-started/installation/), clone the repo, `cd` into
it and execute
```
zola serve
```
Voilà, you now have a little development copy of the site running locally, presumably
on `http://127.0.0.1:1111`. Every time you make a change to the website files, zola
will rebuild it and you will automatically see the newer version.

### Writing articles
To add new articles you must simply create a markdown file in the desired section
within `content`, e.g. `content/commentary/my-post.md` and push it. Make sure it has
a similar header to that of other posts.

Images and other media should be put in `static`.

### How it works
This site uses a fully custom Zola theme, which you can easily fiddle with. The
directory structure is [described in Zola
docs](https://www.getzola.org/documentation/getting-started/directory-structure/).
Briefly speaking:
 - The `templates` directory contains HTML templates described
   [here in the Zola
   documentation](https://www.getzola.org/documentation/templates/overview/). They
   use the [Tera](https://tera.netlify.app/) templating engine.
 - The `sass` directory contains [Sass](https://sass-lang.com/) code, which is
   basically a CSS preprocessor which allows for nice things like nested tag style
   definitions or variables. [Here it is described in Zola
   docs](https://www.getzola.org/documentation/content/sass/).
 - The `static` directory contains things which are static, so left unchanged by the
   whole site building process; things like images and fonts.
 - The `content` directory contains markdown documents which get processed into pages
   in the final site.
